FROM nfcore/hlatyping:1.2.0
LABEL maintainer="marius.herr@uni-tuebingen.de"

# Install Nextflow and create nextflow user
USER root
WORKDIR /tmp
COPY ./requirements.txt /tmp/requirements.txt
RUN apt-get update -yqq && \
    mkdir -p /usr/share/man/man1mkdir -p /usr/share/man/man1 && \
    apt-get install -yqq python3 default-jre\
        python3-pip && \
    apt-get dist-upgrade -yqq && \
    pip3 install --no-cache-dir -r /tmp/requirements.txt && \
    wget -qO- https://get.nextflow.io | bash && \
    mv /tmp/nextflow /usr/local/bin/ && \
    chmod 555 /usr/local/bin/nextflow && \
    useradd -u 1000 -s /bin/bash -m nextflow && \
    mkdir /opt/pht_results && \
    mkdir /opt/pht_train && \
    chown -R nextflow /opt/pht_results && \
    chown -R nextflow /opt/pht_train && \
    rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
        /usr/share/man \
        /usr/share/doc \
        /usr/share/doc-base && sync
# install train container library
COPY ./train_lib /tmp/train-container-library/train_lib
COPY ./setup.py /tmp/train-container-library/setup.py
COPY ./README.md /tmp/train-container-library/README.md
RUN pip install --no-cache-dir /tmp/train-container-library/ && \
    rm -rf /tmp/*

# copy execeution scripts and set entrypoint
COPY ./scripts/security_protocol.py /opt/security/security_protocol.py
COPY ./scripts/entrypoint.sh /opt/entrypoint.sh
ENTRYPOINT ["sh", "/opt/entrypoint.sh"]

USER nextflow
COPY ./scripts/custom_nextflow.config /opt/custom_nextflow.config
RUN nextflow pull nf-core/hlatyping -r 1.2.0
ENV NXF_OFFLINE=TRUE